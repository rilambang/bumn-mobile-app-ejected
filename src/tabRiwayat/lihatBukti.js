import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Image, Linking } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Badge,
    Right,
    Icon,
    Title,
    Input,
    InputGroup,
    Item,
    Tab,
    Tabs,
    Footer,
    FooterTab,
    Label,
    Thumbnail,
    ListItem,
    CheckBox,
    Picker,
    TouchableOpacity
} from "native-base";


import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export default class LihatBukti extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            nomor_laporan: this.props.navigation.state.params.nomor_laporan,
            dataLaporanDetail: [],
            datapetugas: [],
            selected: "key0",
            status: ""
        };
    }

    componentDidMount() {
        console.log(this.state.nomor_laporan)

        fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:3000/uploads1/" + this.state.nomor_laporan, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            },
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data);
                this.setState({
                    dataLaporanDetail: data[0]
                });
                
                console.log(this.state.dataLaporanDetail.id_login);

                fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + this.state.dataLaporanDetail.id_login, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                    },
                })
                    .then(response => response.json())
                    .then((data) => {
                        this.setState({
                            datapetugas: data.data

                        });
                        

                    })
            })

    }


    render() {
        const {goBack} = this.props.navigation;
        return (
            <Container style={{  backgroundColor: "#fff" }}>
                <Header style={{ backgroundColor: "#fff" }}>
                    <Left>
                        <Icon name="arrow-back" onPress={() => goBack(null)} style={{ marginLeft: "5%" }} />

                    </Left>
                    <Body style={{ backgroundColor: "#fff", alignItems: "center" }}>
                        <Title style={{ color: "#000" }}>Detail Laporan</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                   
                    <Image style={{ height: 200, width: null, flex: 1 }} source={{ uri: this.state.dataLaporanDetail.images_evidance }} />
                    
                    <Card>
                        <CardItem>
                            <Left>
                               
                               <Thumbnail small source={{ uri : this.state.datapetugas.profilePicture }} />
                                <Body>
                                    <Text>{this.state.datapetugas.name}</Text>
                                </Body>
                            </Left>
                            <Right>
                                <Text>{this.state.dataLaporanDetail.tanggal_laporan}</Text>
                            </Right>
                        </CardItem>

                        <CardItem>
                            <Text style={{ fontSize: 12 }}>{this.state.dataLaporanDetail.description_evidance} </Text>
                        </CardItem>

                    </Card>
                </Content>

            </Container>

        );
    }



}

const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    },
    oneButton: {
        height: 60,
        backgroundColor: '#e41c23',
    },

    oneButtonView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 20,
        justifyContent: 'center',
    },

    payTextDollar: {
        flex: 1,
        color: '#ffffff',
        fontSize: 20,
        fontFamily: 'MetaOffc-Light',
        textAlign: 'center',
    },

    imageButton: {
        marginTop: 6,
    },
    flexDetailPengerjaan: {
        textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginLeft: "15%", marginRight: "40%", borderRadius: 2
    }
});


