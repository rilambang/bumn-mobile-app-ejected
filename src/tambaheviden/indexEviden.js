import React, { Component } from "react";
import KameraGaleri from "./kameraGaleri.js";
import Detail from "./detail.js";
import BerandaDetail from "../screen/berandaDetail.js";
import Beranda from "../screen/HomeScreen.js";

import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator({
    KameraGaleri: {
        screen: KameraGaleri,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Beranda: {
        screen: Beranda,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    BerandaDetail: {
        screen: BerandaDetail,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Detail: {
        screen: Detail,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    }
}));