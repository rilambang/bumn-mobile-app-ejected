import React from "react";
import { StatusBar, View, MapView, StyleSheet, TouchableHighlight, Linking, TouchableOpacity, AsyncStorage } from "react-native";
import Image from 'react-native-image-progress';
import ProgressCircle from 'react-native-progress/Circle';
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Item

} from "native-base";

import {

	H2,
	Form,
	Label,
	Input,
	List,
	ListItem,

} from "native-base";


import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default class HomeScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            dataLaporan: [],
            page : "1"
         
        }
    }
    componentDidMount() {

        AsyncStorage.getItem("bumnId", (error, result) => {

        fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/getFindBumn/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                instansi : result,
                page : this.state.page
            })
        })
            .then(response => response.json())
            .then((data) => {
                if(data.status==200){
                    this.setState({
							dataLaporan: data.response
						});    
                } else {
                    this.setState({
							dataLaporan: data.response
						});
                }
                console.log(result)

            })
        })
    }

    

    render() {
        
console.log(this.state.dataLaporan);

        const { navigate } = this.props.navigation;
      
           return (
            <Container style={{  }}>
                <Header style={{ backgroundColor: "#fff" }}>
                    <Left></Left>
                    <Body style={{ backgroundColor: "#fff", alignItems: "center" }}>
                        <Title style={{ color: "#000" }}>Sobat BUMN</Title>
                    </Body>
                    <Right>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("SearchPage")}>
                        <Image  source={require("../../img/asset/ico-search.png")} style={{ marginRight: "5%" }} />
                        </TouchableOpacity>
                    </Right>


                </Header>
                <Content padder style={{ backgroundColor: "#f7f7f7" }}  >


                    <Card style={{ backgroundColor: 'powderblue' }}>
                        <CardItem style={{ backgroundColor: 'powderblue' }}>
                            <Left>
                                <Thumbnail square small
                                    source={require("../../img/asset/ico-city.png")}
                                />
                                <Body>
                                    <Text style={{ fontSize: responsiveFontSize(2) }}>Menampilkan Pelaporan-pelaporan disekitar Anda</Text>

                                </Body>
                            </Left>
                        </CardItem>
                    </Card>

                    {
                                this.state.dataLaporan.length == 0 ?
                            <View style ={{marginTop:"10%", alignItems: "center" }}>
                                <Text style={{color:"#cccccc"}}>Belum Ada Laporan Masuk </Text>
                                </View>:null
                        }
        
										
                          { this.state.dataLaporan.length != 0 ?
							this.state.dataLaporan.map((item, index) => (
                            <Card key={item.nomor_laporan}>
                                <TouchableOpacity transparent onPress={() => this.props.navigation.navigate("BerandaDetail", { nomor_laporan: item.nomor_laporan })}>
                                <CardItem>
                                    <Left>
                                        <Thumbnail small
                                            source={{ uri : item.avatar_pelapor}}
                                        />
                                        <Body>
                                            <Text style={{ fontSize: responsiveFontSize(2) }}>{item.pelapor}</Text>
                                            <Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : {item.nomor_laporan}</Text>
                                        </Body>
                                    </Left>
                                    <Right>
                                        <Text style={{ fontStyle: "italic",fontSize: responsiveFontSize(1.5) }}>{item.tanggal_laporan}</Text>
                                    </Right>
                                </CardItem>
                                </TouchableOpacity>
                                <CardItem>

                                    <Text style={{ fontSize: responsiveFontSize(1.8) }}>{item.description}</Text>

                                </CardItem>
                                <CardItem cardBody >

                                    <TouchableOpacity transparent onPress={() => this.props.navigation.navigate("BerandaDetail", { nomor_laporan: item.nomor_laporan })}>
                                        <View >

                                            <Image style={{ height: 200, width: 350 }}
                                                 source={{ uri: item.images }}
                                                 indicator={ProgressCircle} />

                                        </View>
                                    </TouchableOpacity>


                                </CardItem>
                                <CardItem>

                            <View style={{ flex: 1, marginTop: "1%", marginBottom:"1%", width: "100%", height: "100%" }}>
                                {/*<View style={{ backgroundColor: "#000", height: "40%", width: "100%" }}>
                                </View>*/}
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between'
                                }}>
                                    <View style={{  alignItems: 'center', flexDirection: 'row', width: "35%", height: "100%" }} >
                                        <View style={{ alignItems: 'center', marginLeft: "10%", justifyContent: 'center', flexDirection: 'row', width: "20%", height: "50%" }}>
                                            <Image style={{}} source={require("../../img/asset/ic-map.png")} />
                                        </View>
                                        <Text style={{ alignItems: 'center', marginLeft: "10%", fontSize: 10 }}>{item.lokasi}</Text>
                                    </View>

                                    <View style={{  alignItems: 'center', flexDirection: 'row', width: "35%", height: "100%" }} >
                                        <View style={{ borderRadius: 100,backgroundColor:item.icon, alignItems: 'center', marginLeft: "5%", justifyContent: 'center', flexDirection: 'row', width: "10%", height: "35%" }}>
                                            {/*<Image style={{ borderRadius: 100 }} source={require("../../img/asset/ic-menunggu.png")} />*/}
                                        </View>
                                        { item.status == "menunggu solusi" ?
                                            <Text style={{ alignItems: 'center', marginLeft: "5%", fontSize: 10 }}>Menunggu Solusi</Text> : null
                                        }
                                        

                                    </View>
                                </View>
                            </View>
                            {/*<View transparent style={{ flex: 1, height: "100%", flexDirection: 'row', width: "70%", left: "1%" }}>
                                <Image style={{}}
                                    source={require("../../img/asset/ic-map.png")} />
                                <Text note style={{ fontSize: responsiveFontSize(1.5), width: "70%", marginLeft: "5%", marginTop: "-2%" }}>{this.state.dataLaporanDetail.lokasi}</Text>
                            </View>

                            <View transparent style={{ flex: 1, flexDirection: 'row', height: "100%", width: "30%", right: "1%", paddingTop: "5%" }}>
                                <Image style={{ height: "60%", width: "6%" }}
                                    source={require("../../img/asset/ic-menunggu.png")}
                                />
                                <Text note style={{ fontSize: responsiveFontSize(1.5), width: "70%", marginLeft: "5%", marginTop: "0%" }}>{this.state.dataLaporanDetail.status}</Text>
                            </View>*/}
                        </CardItem>
                            </Card>
                            )):null
						}

                </Content>

                <Footer>
                    <FooterTab style={styles.footer}>
                        <Button
                        >

                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-home.png")}
                            />

                            <Text uppercase={false} style={styles.fontFooter}>Beranda</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Riwayat")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-riwayat-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Riwayat</Text>
                        </Button>
                        <Button
                            vertical
                            onPress={() => this.props.navigation.navigate("Bantuan")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-help-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Bantuan</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Profil")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-profile-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Akun</Text>
                        </Button>
                    </FooterTab>
                </Footer>

            </Container>
        );
    }
   
};






const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    }
})

