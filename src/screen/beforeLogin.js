import React from "react";
import { Platform, StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground } from "react-native";
import {
    Button,
    View,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item as FormItem,
    Label,
    Input,
    Picker,

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import OneSignal from 'react-native-onesignal'; // Import package from node modules

const Item = Picker.Item;



export default class BeforeLogin extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dataBumn: [],
            selected2: undefined,
            playerId: ""
        }
    }

    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }
     componentWillMount() {
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('registered', this.onRegistered);
         OneSignal.addEventListener('ids', this.onIds);
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('registered', this.onRegistered);
        OneSignal.removeEventListener('ids', this.onIds);
    }

    onReceived(notification) {
        console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
    }

    onRegistered(notifData) {
        console.log("Device had been registered for push notifications!", notifData);
    }

    onIds(device) {
		console.log('Device info: ', device.userId);
        AsyncStorage.setItem("playerId",device.userId);
          AsyncStorage.getItem("playerId", (error, result) => {
                 console.log('id info : ', result);
          })
       
    }
    componentDidMount() {
       

        fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/list-bumn", {
            method: "GET",
            headers: {
                'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            }
        })
            .then((response) => response.json())
            .then((data) => {

                this.setState({
                    dataBumn: data.data,
                });
            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {
       return (
            <Container>

                <ImageBackground source={require('../../img/icon/regular/signin-bg.jpg')} style={{
                    flex: 1,
                    width: null,
                    height: null
                }}>


                    <Content padder style={{}}  >



                        <Form>
                            {/* <Text style={{ height: 450 }} /> */}
                            {/*<Image source={{ uri: "url(/my-image.png)" }}
                            style={{ height: 250, width: 250, marginLeft: "33%", flex: 1, marginTop: 75, marginBottom: 75 }} />*/}

                            <Label style={{ marginTop: "50%", textAlign: 'center', fontSize: responsiveFontSize(3), color: "white" }}>Sebelum masuk, sebutkan asal BUMN Anda</Label>



                            {/*<FormItem style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", paddingRight: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "15%", height: responsiveHeight(7), borderRadius: 5 }}>
                                <Input style={{ color: "white" }} placeholderTextColor="white" placeholder="Ketik nama" />
                                <Image source={require('../../img/asset/ic-search.png')} />
                            </FormItem>*/}


                            <Picker
                                style={{ justifyContent: 'center', alignItems: 'center',backgroundColor: 'rgba(255, 255, 255, 1)', paddingLeft: "3%", paddingRight: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "15%", height: responsiveHeight(7), width: responsiveHeight(43), borderRadius: 5 }}
                                mode="dropdown"
                                placeholder="Pilih satu"
                                selectedValue={this.state.selected2}
                                onValueChange={this.onValueChange2.bind(this)}
                            >
                           
                                {
                                        
                                    this.state.dataBumn.map((item, index) => (
                                       
                                        <Item style={{ color: "white" }} label={item.bumnName} value={item.bumnId} key={item._id}/>

                                    ))
                                }
                                
                            </Picker>

                            <Button bordered light style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%", marginRight: "30%", marginLeft: "30%", width: responsiveWidth(40) }}
                                onPress={this.validasiBumn} >
                                <Text style={{}}>Pilih</Text>
                            </Button>

                         
                        </Form>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }

    validasiBumn = () => {
        if (this.state.selected2 == null || this.state.selected2 == "") {
            Alert.alert(
                'Pesan',
                'Harap pilih BUMN terlebih dahulu',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                        }
                    },
                ],
                { cancelable: false }
            )
        } else {
            console.log(this.state.selected2)
            AsyncStorage.setItem("bumnId",this.state.selected2);
            this.props.navigation.navigate("Login", { bumnId: this.state.selected2 });
        }
    }
}



