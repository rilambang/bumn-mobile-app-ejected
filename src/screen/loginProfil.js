import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';





export default class LoginProfil extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dataUser: [],
            userId: ""
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("userid", (error, result) => {
            if (result) {
                console.log("userid : " + result)
            }
            this.state.userId = result;
            fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + result,{
                method: "GET",
                headers: {
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                }
            })
                .then((response) => response.json())
                .then((data) => {

                    this.setState({
                        dataUser: data.data,
                    });

                    console.log(this.state.dataUser)
                })
                .catch((error) => {
                    console.log(error);
                })
        })
    }

    render() {
        return (
            <Container style={{
                justifyContent: 'center',
                alignItems: 'center',
            }}>

                <ImageBackground source={require('../../img/icon/regular/signin-bg.jpg')} style={{
                    flex: 1,
                    width: null,
                    height: null,

                    alignItems: 'center'
                }} >
                    <Thumbnail large source={{uri : this.state.dataUser.profilePicture}} style={{ zIndex: 2, marginTop: "37%", marginRight: "50%", marginLeft: "50%" }} />
                    <Content padder style={{ position: "absolute", flex: 1, backgroundColor: 'rgba(255, 255, 255, 1)', marginRight: "10%", marginLeft: "10%", marginTop: "50%", marginBottom: "50%", borderRadius: 10 }}  >




                        <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(2), color: "black", marginTop: "25%" }}>
                            Selamat Bertugas,
                                </Label>
                        <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(2), color: "black", fontWeight: "bold" }}>
                            {this.state.dataUser.name}
                                </Label>


                        <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginTop: "4%" }}>
                            {this.state.dataUser.nik}
                                </Label>

                        <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginTop: "4%", backgroundColor: '#e8f0ff', marginLeft: "30%", marginRight: "30%", borderRadius: 2 }}>
                            {this.state.dataUser.bumnName}
                                </Label>

                        <Button onPress={() => this.props.navigation.navigate("Beranda")} primary style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: "25%", marginRight: "25%", marginTop: "15%", width: responsiveWidth(40), borderRadius: 10 }}><Text> Lanjut </Text></Button>



                        {/*<Label style={{ marginTop: "40%", textAlign: 'center', fontSize: responsiveFontSize(4), color: "white" }}>Selamat Datang!</Label>

                        <Item style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "15%", height: responsiveHeight(7), borderRadius: 5 }}>
                            <Icon style={{ paddingRight: "5%" }} active name='home' />
                            <Input style={{ color: "white" }} placeholderTextColor="white" placeholder="NIK" />
                        </Item>

                        <Item style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', paddingLeft: "3%", marginRight: "10%", marginLeft: "10%", marginTop: "3%", height: responsiveHeight(7), borderRadius: 5 }}>
                            <Icon style={{ paddingRight: "5%" }} active name='home' />
                            <Input style={{ color: "white" }} placeholderTextColor="white" placeholder="Password" />
                        </Item>

                        <Label style={{ marginTop: "1%", textAlign: 'right', fontSize: responsiveFontSize(2), color: "white", marginRight: "10%", fontStyle: "italic" }} >Lupa Password</Label>

                        <Button bordered light style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%",marginRight:"30%",marginLeft:"30%",width:responsiveWidth(40)}} >
                            <Text style={{}}>Masuk</Text>
                        </Button>

                        <Label style={{ marginTop:"40%", textAlign: 'center', fontSize: responsiveFontSize(2), color: "white", fontStyle: "italic" }} >Tidak bisa masuk? Hubungi kami</Label>*/}




                    </Content>
                </ImageBackground>
            </Container>
        );
    }
}



