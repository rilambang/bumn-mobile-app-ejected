import React, { Component } from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import { Container, Content, Picker, Button, Text } from "native-base";

import BeforeLogin from "./src/screen/index.js";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  async componentWillMount() {

    this.setState({ isReady: true });
  }
  render() {
    if (this.state.isReady == false) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    } else {
      return (
        <BeforeLogin />
      );
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
